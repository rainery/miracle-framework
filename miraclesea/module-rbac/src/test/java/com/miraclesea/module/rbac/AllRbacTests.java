package com.miraclesea.module.rbac;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.miraclesea.module.rbac.controller.ApiRoleControllerTest;
import com.miraclesea.module.rbac.dao.RoleDaoTest;
import com.miraclesea.module.rbac.entity.RoleTest;
import com.miraclesea.module.rbac.service.RoleServiceTest;
import com.miraclesea.module.rbac.vo.PermissionUtilTest;

@RunWith(Suite.class)
@SuiteClasses({
	PermissionUtilTest.class, 
	RoleTest.class, 
	RoleDaoTest.class, 
	RoleServiceTest.class, 
	ApiRoleControllerTest.class
})
public final class AllRbacTests { }
