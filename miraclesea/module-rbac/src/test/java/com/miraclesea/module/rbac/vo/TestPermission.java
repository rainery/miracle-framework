package com.miraclesea.module.rbac.vo;

enum TestPermission implements Permission {
	
	TEST;
	
	@Override
	public String toPermissionString() {
		return PermissionUtil.toPermissionString(this);
	}
}
